#!/bin/bash
readonly PYV_ROOT=.
readonly CONFIG_ROOT=/usr/local/etc/pyv

set -eo pipefail
# TODO: write error messages to stderr instead of stdout

. ${PYV_ROOT}/lib.sh


source_virtualenvwrapper() {
    local virtualenvwrapper_path

    test_virtualenvwrapper() {
        function_exists mkvirtualenv &&
        function_exists rmvirtualenv
        # TODO: add all virtualenvwrapper commands pyv will use

        if [ $? -ne 0 ]; then
            echo "sourced $virtualenvwrapper_path but virtualenvwrapper was not loaded properly."
            return 78
        fi
        return 0
    }

    # already sourced
    if test_virtualenvwrapper > /dev/null; then
        return 0
    fi

    # from config
    local ret=0
    virtualenvwrapper_path=$(pyv_config get virtualenvwrapper-path) || ret=$?
    if [ $ret -eq 0 ]; then
        if [ -f "$virtualenvwrapper_path" ]; then
            . "$virtualenvwrapper_path"
            test_virtualenvwrapper
            return
        else
            echo "virtualenvwrapper-path is set in config but the file does not exist."
            echo "use \`pyv config set virtualenvwrapper-path <value>\` to fix this if you already have " \
                 "virtualenvwrapper installed on your machine."
            # TODO: explain how to use pyv to install virtualenvwrapper
            return 78
        fi
    fi

    # from $PATH
    virtualenvwrapper_path=$(which virtualenvwrapper.sh) || true
    if [ -f "$virtualenvwrapper_path" ]; then
        . "$virtualenvwrapper_path"
        test_virtualenvwrapper
        return
    fi

    # not found
    echo "virtualenvwrapper.sh was not found in \$PATH. Either install it in the \$PATH or set the " \
         "virtualenvwrapper-path field with \`pyv config\`"
    return 78
}


# TODO: support other versions supported py pyenv (e.g. pypy)
valid_version_format() {
    version_arr=(${1//./ })  # split by .
    for val in "${version_arr[@]}"; do
        if ! is_numeric "$val"; then
            return 1
        fi
    done
    return 0
}


unrecognized() {
    echo "unrecognized command: $1"
    echo "run \`pyv help\` for a list of commands"
}

pyv_config() (

    unrecognized() {
        echo "unrecognized command: $1"
        echo "run \`pyv help config\` for a list of commands"
    }

    readonly CONST_FIELD_NAMES=(
        system-python
        secondary-system-python
        virtualenvwrapper-path
    )

    valid_fieldname() {
        arr_contains "$1" "${CONST_FIELD_NAMES[@]}"
        # exit code of function is exit code of last command
    }

    invalid_fieldname() {
        echo "invalid field name: $1"
        # TODO: have a way to list valid field names
        echo "see \`pyv help config\` for more information"
    }

    pyv_config_get() {
        if ! valid_fieldname "$1"; then
            invalid_fieldname "$1"
            return 65
        fi

        if [ ! -f "${CONFIG_ROOT}/${1}" ]; then
            echo "field $1 is not set"
            return 78
        fi
        cat "${CONFIG_ROOT}/${1}"
    }

    pyv_config_set() {
        if ! valid_fieldname "$1"; then
            invalid_fieldname "$1"
            return 65
        fi
        # TODO: check for valid value
        echo $2 > "${CONFIG_ROOT}/${1}"
    }

    pyv_config_rm() {
        if ! valid_fieldname "$1"; then
            invalid_fieldname "$1"
            return 65
        fi
        # TODO: maybe add warning
        rm -f "${CONFIG_ROOT}/${1}"
    }

    case $1 in
        get)
            pyv_config_get "${@:2}"
            ;;
        set)
            pyv_config_set "${@:2}"
            ;;
        rm)
            pyv_config_rm "${@:2}"
            ;;
        "")
            for filepath in "${CONFIG_ROOT}"/*; do
                if [ ! -f "$filepath" ]; then
                    continue
                fi
                fieldname=$(basename "$filepath")
                if ! valid_fieldname "$fieldname"; then
                    continue
                fi
                echo $fieldname=$(cat "$filepath")
            done
            ;;
        *)
            unrecognized "$1"
    esac
)

pyv_help() {
    case $1 in
        "")
            cat "${PYV_ROOT}/docs/pyv-help.txt"
            ;;
        config)
            cat "${PYV_ROOT}/docs/pyv-config-help.txt"
            ;;
        help)
            cat "${PYV_ROOT}/docs/pyv-help-help.txt"
            ;;
        mk)
            cat "${PYV_ROOT}/docs/pyv-mk-help.txt"
            ;;
        rm)
            cat "${PYV_ROOT}/docs/pyv-rm-help.txt"
            ;;
        setup)
            cat "${PYV_ROOT}/docs/pyv-setup-help.txt"
            ;;
        version)
            cat "${PYV_ROOT}/docs/pyv-version-help.txt"
            ;;
        *)
            unrecognized "$1"
    esac
}

pyv_mk() (
    source_virtualenvwrapper

    pushd ${PYV_ROOT}/pyenv/versions > /dev/null
        shopt -s nullglob
            versions=(*)
        shopt -u nullglob
    popd > /dev/null
    versions+=($(pyv_config get system-python))
    if pyv_config get secondary-system-python > /dev/null; then  # field is set
        versions+=($(pyv_config get secondary-system-python))
    fi

    # parse args
    positional=()
    while [ -n "$1" ]; do
        case $1 in
            -v|--version)
                shift
                version=$1
                ;;
            --)
                shift
                positional+=("$@")
                break
                ;;
            -*)
                echo "unrecognized option: $1"
                echo "See \`pyv help mk\`"
                return 64
                ;;
            *)
                positional+=("$1")
        esac
        shift
    done

    if [ -z "${positional[0]}" ]; then
        echo "positional argument <name> is required"
        echo 'see `pyv help mk` for more info'
        return 64
    fi

    if [ -n "$version" ] && ! valid_version_format "$version"; then
        echo "invalid version format: $version"
        return 65
    fi

    version_arr=(${version//./ })
    if [ ${#version_arr[@]} -lt 3 ]; then
        if [ -z $version ]; then
            version="*"
        elif [ "${version_arr[-1]}" != "*" ]; then
            version+=".*"
        fi
    fi
    versions=($(filter_arr "$version" "${versions[@]}"))

    if [ ${#versions[@]} -eq 0 ]; then
        echo "no matching versions installed: $version"
        echo 'run `pyv v ls` to see a list of currently installed versions'
        return 1
    fi

    versions=($(printf '%s\n' "${versions[@]}" | sort --version-sort))
    echo "matching versions: ${versions[*]}"
    version=${versions[-1]}

    if [ -d "${PYV_ROOT}/pyenv/versions/$version" ]; then
        python_path=${PYV_ROOT}/pyenv/versions/${version}/bin/python
    else
        python_path=$(which python${version::1})
    fi

    echo "using version $version at '$python_path'"

    # TODO: error if virtualenv already exists

    # https://bitbucket.org/virtualenvwrapper/virtualenvwrapper/issues/329
    mkvirtualenv --python="$python_path" "${positional[0]}"
)

pyv_rm() {
    source_virtualenvwrapper

    rmvirtualenv "$1"
}

pyv_setup() (
    unrecognized() {
        echo "unrecognized command: $1"
        echo "run \`pyv help setup\` for a list of commands"
    }

    pyv_setup_install-pip() {
        if [ -n "$(which pip3)" ]; then
            echo "pip is already installed"
            return 78
        fi
        curl https://bootstrap.pypa.io/get-pip.py | python
    }

    pyv_setup_install-virtualenvwrapper() {
        # todo: add --hidden option
        if function_exists mkvirtualenv; then
            echo "virtualenvwrapper is already installed"
            return 78
        fi
        if ! python3 -m pip > /dev/null 2>&1; then
            echo "pip is not installed for the following python version:"
            python3 --version
            echo "either run \`pyv setup install-pip\` or install pip yourself"
            return 78
        fi
        python3 -m pip install virtualenvwrapper
    }

    pyv_setup_uninstall-pyv() {
        # TODO: add 'are you sure' and -y bypass
        while [ -n "$(which pyv)" ]; do
            rm $(which pyv)
        done
        rm -rf $PYV_ROOT
        rm -rf $CONFIG_ROOT
        echo "successfully uninstalled pyv"
    }

    # TODO: add upgrade-pyv command

    case $1 in
        install-pip)
            pyv_setup_install-pip "${@:2}"
            ;;
        install-virtualenvwrapper)
            pyv_setup_install-virtualenvwrapper "${@:2}"
            ;;
        uninstall-pyv)
            pyv_setup_uninstall-pyv "${@:2}"
            ;;
        *)
            unrecognized "$1"
    esac
)

pyv_v() (
    unrecognized() {
        echo "unrecognized command: $1"
        echo 'run `pyv help version` for a list of commands'
    }

    pyv_v_install() (
        version="$1"

        invalid_version() {
            echo "invalid version: $version"
        }

        if ! valid_version_format "$version"; then
            invalid_version
            return 65
        fi

        export PYENV_ROOT=${PYV_ROOT}/pyenv

        # check if version exists
        if [ -z "$(pyv_v_ls --all | grep -x $version)" ]; then
            invalid_version
            return 65
        fi

        ${PYENV_ROOT}/bin/pyenv install $version
    )

    pyv_v_ls() (
        all=1  # false
        # parse args
        positional=()
        while [ -n "$1" ]; do
            case $1 in
                -a|--all)
                    all=0  # true
                    ;;
                --)
                    shift
                    positional+=("$@")
                    break
                    ;;
                -*)
                    echo "unrecognized option: $1"
                    echo 'See `pyv help version`'
                    return 64
                    ;;
                *)
                    positional+=("$1")
            esac
            shift
        done

        export PYENV_ROOT=${PYV_ROOT}/pyenv
        if [ $all -ne 0 ]; then
            echo "$(pyv_config get system-python) (system)"
            if pyv_config get secondary-system-python > /dev/null; then  # field is set
                echo "$(pyv_config get secondary-system-python) (system secondary)"
            fi
            ${PYENV_ROOT}/bin/pyenv versions --bare
        else
            ${PYENV_ROOT}/bin/pyenv install --list | while read -r i; do
                if valid_version_format "$i"; then
                    echo $i
                fi
            done
        fi
    )

    case $1 in
        install)
            pyv_v_install "${@:2}"
            ;;
        ls)
            pyv_v_ls "${@:2}"
            ;;
        *)
            unrecognized "$1"
    esac
)

case $1 in
    config)
        pyv_config "${@:2}"
        ;;
    help)
        pyv_help "${@:2}"
        ;;
    mk)
        pyv_mk "${@:2}"
        ;;
    rm)
        pyv_rm "${@:2}"
        ;;
    setup)
        pyv_setup "${@:2}"
        ;;
    v)
        pyv_v "${@:2}"
        ;;
    *)
        unrecognized "$1"
esac
