#!/bin/bash
#
# generic bash utilities

# bash's $'\n' is not POSIX
# NEWLINE="
# "

# TODO: replace arrays with POSIX


function_exists() {
    declare -f -F $1 > /dev/null
    # exit code of function is exit code of last command
}


filter_arr() (
    match=$1
    shift
    for val in "$@"; do
        case "$val" in
            $match)
                echo "$val"
        esac
    done
)


arr_contains() (
    match="$1"
    shift
    for val in "$@"; do
        if [ $val == $match ]; then
            return 0
        fi
    done
    return 1
)

is_numeric() {
    case "$1" in
        ""|*[!0-9]*)
            return 1
            ;;
        *)
            return 0
            ;;
    esac
}
