# pyv

Python environment manager (wraps `virtualenvwrapper` and `pyenv`).

WORK IN PROGRESS

---

**Dependencies** (for Debian):
* `bash` (`>=4.3.48` is safe, although it may work on earlier versions)
* `curl`
* `git`
* `python3` (`>=3.5.2` is safe, although it may work on earlier versions)
* `python` (new enough that `get-pip.py` will take it) (can be Python 3)
* `python-distutils` (or `python3-distutils` if you used Python 3 for the `python` dependency)

---

**Install instructions:**

1. Install dependencies (using `apt` or something).

2. Clone this repository (anywhere).

```bash
git clone https://gitlab.com/nog642/pyv.git
```

3. Run the install script with root privileges.

```bash
sudo python3 pyv/install.py
```

4. (optional) Delete the cloned repository, as it is not needed anymore.

```bash
rm -rf pyv
```

**Uninstall instructions:**

1. Run the uninstall command with root privileges.

```bash
sudo pyv setup uninstal-pyv
```

2. Restart your shell.
