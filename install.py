#!/usr/bin/env python3
import argparse
import os
import shutil
import stat
import subprocess
import sys

LIB_ROOT = '/usr/local/lib/pyv'
BIN_ROOT = '/usr/local/bin'
SELF_ROOT = os.path.dirname(os.path.realpath(__file__))


def existing_install():
    # TODO: maybe use stderr instead of stdout
    print('There already seems to be an installation of pyv on this machine.')
    print('If you wish to uninstall the existing installation, try running '
          '`pyv setup uninstall-pyv`, or if that fails, `bash {}/pyv.sh setup '
          'uninstall-pyv`.'.format(LIB_ROOT))
    print('If the current installation is broken and you wish to override it '
          'with a new installation, run this script again with the --override '
          'flag.')
    sys.exit(78)  # https://www.freebsd.org/cgi/man.cgi?query=sysexits


def python_version(executable):
    return subprocess.check_output(
        (executable, '-c',
         "import sys\nprint('.'.join(map(str, sys.version_info[:3])))")
    ).strip().decode('ascii')


argparser = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    usage="install.py [-h|--help] [--override [--hard]] [--configroot CONFIGROOT]"
)
argparser.add_argument('--override', action='store_true',
                       help="override existing pyv installation")
argparser.add_argument('--hard', action='store_true',
                       help="override pyv's existing pyenv installation (thus "
                            "losing all versions of python installed with "
                            "pyv)")
argparser.add_argument('--configroot', default='/usr/local/etc',
                       help='Where to install config files. A subdirectory '
                            'called pyv will be created here.')
args = argparser.parse_args()

if args.hard and not args.override:
    argparser.error("the --hard argument can only be used with the --override"
                    " argument")

config_root = os.path.join(args.configroot, 'pyv')


def write_config(field_name, value):
    with open(os.path.join(config_root, field_name), 'w') as f:
        f.write(value + '\n')


def main():
    print("~~~ creating lib root at '{}' ~~~".format(LIB_ROOT))
    clone_pyenv = True
    try:
        os.makedirs(LIB_ROOT)
    except FileExistsError:
        if args.override:
            if args.hard:
                shutil.rmtree(LIB_ROOT)
                os.makedirs(LIB_ROOT)
            else:
                for sub in os.listdir(LIB_ROOT):
                    sub_full = os.path.join(LIB_ROOT, sub)
                    if os.path.isdir(sub_full):
                        if sub == "pyenv":
                            clone_pyenv = False
                        else:
                            shutil.rmtree(sub_full)
                    else:
                        os.remove(sub_full)
        else:
            existing_install()

    print("~~~ creating config root at '{}' ~~~".format(config_root))
    try:
        os.makedirs(config_root)
    except FileExistsError:
        if args.override:
            if args.hard:
                shutil.rmtree(config_root)
                os.makedirs(config_root)
            else:
                print("not overriding existing config (pass --hard to do so)")
        else:
            existing_install()

    if clone_pyenv:
        print("~~~ cloning pyenv into '{}' ~~~".format(os.path.join(LIB_ROOT,
                                                                    'pyenv')))
        # TODO: hardcode pyenv version
        # TODO: depth 1
        git_clone_exit_code = subprocess.call(
            ('git', 'clone', 'https://github.com/pyenv/pyenv.git'),
            cwd=LIB_ROOT
        )
        if git_clone_exit_code != 0:
            # TODO: clean up
            raise RuntimeError('git clone failed with exit code '
                               '{}'.format(git_clone_exit_code))
        os.mkdir(os.path.join(LIB_ROOT, 'pyenv', 'versions'))
    else:
        print("~~~ existing pyenv found at '{}'; not recloning ~~~".format(
            os.path.join(LIB_ROOT, 'pyenv')
        ))

    print("~~~ copying bash scripts to lib root ~~~")
    with open(os.path.join(SELF_ROOT, 'src', 'pyv.sh')) as f:
        contents = f.readlines()
    # hardcode installation paths into bash script to be installed
    contents[1] = 'readonly PYV_ROOT={}\n'.format(LIB_ROOT)
    contents[2] = 'readonly CONFIG_ROOT={}\n'.format(config_root)
    contents = ''.join(contents)
    pyv_sh_dest = os.path.join(LIB_ROOT, 'pyv.sh')
    with open(pyv_sh_dest, 'w') as f:
        f.write(contents)
    os.chmod(pyv_sh_dest, stat.S_IRWXU | stat.S_IRGRP | stat.S_IXGRP |
             stat.S_IROTH | stat.S_IXOTH)
    shutil.copy(os.path.join(SELF_ROOT, 'src', 'lib.sh'), LIB_ROOT)

    print("~~~ copying documentation to lib root ~~~")
    shutil.copytree(os.path.join(SELF_ROOT, 'docs'),
                    os.path.join(LIB_ROOT, 'docs'))

    pyv_bin_dest = os.path.join(BIN_ROOT, 'pyv')
    print("~~~ creating '{}' ~~~".format(pyv_bin_dest))
    try:
        os.symlink(pyv_sh_dest, pyv_bin_dest)
    except FileExistsError:
        if args.override:
            os.remove(pyv_bin_dest)
            os.symlink(pyv_sh_dest, pyv_bin_dest)
        else:
            existing_install()

    print("~~~ initializing configuration ~~~")
    system_version = python_version('python')
    print("~~~     primary system python version detected: {} ~~~".format(
        system_version
    ))
    system_major_version = int(system_version.split('.')[0])
    # TODO: handle if there is no secondary python
    if system_major_version == 2:
        secondary_system_version = python_version('python3')
    elif system_major_version == 3:
        secondary_system_version = python_version('python2')
    else:
        raise RuntimeError('wtf dude')
    print("~~~     secondary system python version detected: {} ~~~".format(
        secondary_system_version
    ))
    write_config('system-python', system_version)
    write_config('secondary-system-python', secondary_system_version)


main()
