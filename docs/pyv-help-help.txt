pyv help [<command>]
    print help information about command given, or pyv in general if command is
    not given.
